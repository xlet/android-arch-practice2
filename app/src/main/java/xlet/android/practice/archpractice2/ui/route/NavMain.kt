package xlet.android.practice.archpractice2.ui.route

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import xlet.android.practice.archpractice2.ui.route.Route.UserAvatar
import xlet.android.practice.archpractice2.ui.route.Route.UserVideo
import xlet.android.practice.archpractice2.ui.screen.useravatar.UserAvatarScreen
import xlet.android.practice.archpractice2.ui.screen.uservideo.UserVideoScreen

private const val ID = "id"
private const val USER_ID = "userId"
private const val USER_AVATAR = "avatar?$USER_ID=%s"
private const val USER_VIDEO = "video?$ID=%s"

object Route {
    fun UserAvatar(userId: String = "{$USER_ID}") = USER_AVATAR.format(userId)
    fun UserVideo(id: String = "{$ID}") = USER_VIDEO.format(id)
}

@Composable
fun NavMain(
    modifier: Modifier = Modifier,
    navController: NavHostController = rememberNavController(),
) {
    NavHost(
        modifier = modifier,
        navController = navController,
        startDestination = UserAvatar(),
    ) {
        composable(
            route = UserAvatar(),
            arguments = listOf(
                navArgument(USER_ID) { defaultValue = "" },
            ),
        ) {
            UserAvatarScreen(
                onNavigateToVideo = {
                    navController.navigate(UserVideo(it))
                },
            )
        }
        composable(
            route = UserVideo(),
            arguments = listOf(
                navArgument(ID) { defaultValue = "" },
            ),
        ) { backStackEntry ->
            UserVideoScreen(backStackEntry.arguments?.getString(ID).orEmpty())
        }
    }
}
