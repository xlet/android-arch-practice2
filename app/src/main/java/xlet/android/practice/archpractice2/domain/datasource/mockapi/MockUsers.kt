package xlet.android.practice.archpractice2.domain.datasource.mockapi

import androidx.core.net.toUri
import xlet.android.practice.archpractice2.data.User
import xlet.android.practice.archpractice2.data.Video
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
internal class MockUsers @Inject constructor() {
    fun getMe(): User {
        return User(
            id = "aa",
            name = "Bob",
            avatarImageUri = "https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png".toUri(),
        )
    }

    fun getMyVideo(): Video {
        return Video(
            id = "vv",
            sourceUri = "http://distribution.bbb3d.renderfarming.net/video/mp4/bbb_sunflower_1080p_60fps_normal.mp4".toUri(),
        )
    }
}
