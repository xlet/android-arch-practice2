package xlet.android.practice.archpractice2

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PracticeApplication : Application()