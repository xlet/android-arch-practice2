package xlet.android.practice.archpractice2.di

import android.content.Context
import androidx.startup.AppInitializer
import androidx.work.WorkManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import xlet.android.practice.archpractice2.initializer.WorkManagerInitializer
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppStartUpModule {
    @Provides
    @Singleton
    fun createWorkManager(
        @ApplicationContext context: Context
    ): WorkManager {
        return AppInitializer.getInstance(context)
            .initializeComponent(WorkManagerInitializer::class.java)
    }
}
