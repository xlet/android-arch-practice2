package xlet.android.practice.archpractice2.ui.screen.useravatar

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.SharingStarted.Companion.Lazily
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.shareIn
import xlet.android.practice.archpractice2.domain.repository.UserRepository
import javax.inject.Inject

@HiltViewModel
class UserAvatarViewModel @Inject constructor(
    private val userRepository: UserRepository,
) : ViewModel() {
    private val user by lazy { userRepository.getMeFlow().shareIn(viewModelScope, Lazily) }
    val userId by lazy { user.map { it.id } }
    val avatarUri by lazy { user.map { it.avatarImageUri } }
}
