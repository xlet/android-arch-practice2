package xlet.android.practice.archpractice2.data

import android.net.Uri

data class User(
    val id: String,
    val name: String,
    val avatarImageUri: Uri,
)
