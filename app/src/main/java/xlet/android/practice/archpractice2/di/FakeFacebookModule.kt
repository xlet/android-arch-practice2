package xlet.android.practice.archpractice2.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import timber.log.Timber

@Module
@InstallIn(SingletonComponent::class)
object FakeFacebookModule {

    @Provides
    fun fakeFacebookProvider() = FakeFacebookSdk()
}

class FakeFacebookSdk {
    init {
        Timber.d("new FakeFacebookSdk")
    }

    fun init() {
        Timber.d("init FakeFacebookSdk")
    }
}
