package xlet.android.practice.archpractice2.data

import android.net.Uri

data class Video(
    val id: String,
    val sourceUri: Uri,
)