package xlet.android.practice.archpractice2.domain.repository.api

import kotlinx.coroutines.flow.Flow
import xlet.android.practice.archpractice2.data.User
import xlet.android.practice.archpractice2.data.Video

interface UserRepositoryApi {
    fun getMeFlow(): Flow<User>

    fun getMyVideo(): Flow<Video>
}
