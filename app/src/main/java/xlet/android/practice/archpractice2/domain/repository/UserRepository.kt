package xlet.android.practice.archpractice2.domain.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import xlet.android.practice.archpractice2.data.User
import xlet.android.practice.archpractice2.data.Video
import xlet.android.practice.archpractice2.domain.datasource.mockapi.MockUsers
import xlet.android.practice.archpractice2.domain.repository.api.UserRepositoryApi
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject internal constructor(
    private val mockUsers: MockUsers,
) : UserRepositoryApi {
    override fun getMeFlow(): Flow<User> {
        return flowOf(mockUsers.getMe())
    }

    override fun getMyVideo(): Flow<Video> {
        return flowOf(mockUsers.getMyVideo())
    }
}
