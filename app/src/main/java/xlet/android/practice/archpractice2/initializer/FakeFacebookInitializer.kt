package xlet.android.practice.archpractice2.initializer

import android.content.Context
import androidx.startup.Initializer
import timber.log.Timber
import xlet.android.practice.archpractice2.di.FakeFacebookSdk
import xlet.android.practice.archpractice2.di.InitializerEntryPoint
import javax.inject.Inject

class FakeFacebookInitializer : Initializer<FakeFacebookSdk> {
    @Inject
    lateinit var fakeFacebookSdk: FakeFacebookSdk

    override fun create(context: Context): FakeFacebookSdk {
        InitializerEntryPoint.resolve(context).inject(this)
        fakeFacebookSdk.init()
        Timber.d("FakeFacebookSdk initialized")
        return fakeFacebookSdk
    }

    override fun dependencies(): List<Class<out Initializer<*>>> {
        return listOf(DependencyGraphInitializer::class.java, TimberInitializer::class.java)
    }
}