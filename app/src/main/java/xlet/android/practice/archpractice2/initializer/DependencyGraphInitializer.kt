package xlet.android.practice.archpractice2.initializer

import android.content.Context
import androidx.startup.Initializer
import timber.log.Timber
import xlet.android.practice.archpractice2.di.InitializerEntryPoint

class DependencyGraphInitializer : Initializer<Unit> {

    override fun create(context: Context) {
        // this will lazily initialize ApplicationComponent before Application's `onCreate`
        InitializerEntryPoint.resolve(context)
        Timber.d("DependencyGraphInitializer created")
    }

    override fun dependencies(): List<Class<out Initializer<*>>> {
        return listOf(TimberInitializer::class.java)
    }
}